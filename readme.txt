Rocrail in Fremo - Benutzerdefinierte Konzepte, Konfigurationen und XML-Skripte

Dieses Git-Repository enthält selbst erstellte Bedienkonzepte, Konfigurationen und XML-Skripte für die Modellbahnsteuerungssoftware Rocrail. 
Die hier bereitgestellten Werke stehen unter der Creative Commons Namensnennung-NichtKommerziell 4.0 International Lizenz (CC BY-NC 4.0).
https://creativecommons.org/licenses/by-nc/4.0/legalcode.de

Lizenzinformationen: CC BY-NC 4.0


Über Rocrail

Rocrail ist eine leistungsstarke Modellbahnsteuerungssoftware, die es ermöglicht, Modelleisenbahnen digital zu steuern und zu automatisieren. 
Rocrail selbst unterliegt den Bedingungen, die auf der offiziellen Rocrail Credits-Seite beschrieben sind.
https://wiki.rocrail.net/doku.php?id=credits-en

Inhalt des Repositories

In diesem Verzeichnis finden Sie selbst erstellte Bedienkonzepte oder Konfigurationen für Rocrail in Form einer xml Plan-Datei, die Sie in Ihren Plan importieren können.
Diese Konzepte dienen dazu, die Benutzeroberfläche der Software zu verbessern und an individuelle Anforderungen anzupassen.

Das Verzeichnis /scripte enthält selbst erstellte XML-Skripte, die spezifische Funktionen oder Automatisierungen für Rocrail implementieren. 
Diese Skripte können in Ihre eigene Rocrail-Installation integriert und angepasst werden.

Verwendung

Bitte beachten Sie die Lizenzbedingungen (CC BY-NC 4.0), bevor Sie die in diesem Repository enthaltenen Werke verwenden, modifizieren oder teilen. 
Sie sind herzlich willkommen, die Dateien für den eigenen, privaten Gebrauch weiter zu verwenden und abzuwandeln. 
Bitte kontaktieren Sie mich für kommerzielle Nutzung.
